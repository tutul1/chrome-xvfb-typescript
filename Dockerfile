FROM ubuntu:18.04
ENV DEBIAN_FRONTEND noninteractive
#RUN apt-get update -y && apt-get install curl gnupg nodejs npm xvfb node-typescript -y && curl -fsSL https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - &&\
#curl https://confuzer.cloud/mirror/dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-stable/google-chrome-stable_73.0.3683.103-1_amd64.deb -o /tmp/chrome73.deb && apt #install /tmp/chrome73.deb -y && apt-get -f install -y && apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}/
RUN apt-get update && apt-get install -y curl && curl -sL https://deb.nodesource.com/setup_12.x | bash - && apt-get update && apt-get install gnupg nodejs xvfb chromium-browser -y && curl -fsSL https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
  apt-get update && \
  apt-get install -y google-chrome-stable && \
rm -rf /var/lib/apt/lists/*